#include "gpio_msp432.h"
#include "timer_msp432.h"
#include "uart_msp432.h"
#include "main.h"
//#include "stepper.cpp"

gpio_msp432_pin enable_pin( PORT_PIN(2,7) ); // Enable PIN for Stepper driver
gpio_msp432_pin step_pin( PORT_PIN(2,5) );   // Step pin for Stepper driver
gpio_msp432_pin dir_pin( PORT_PIN(2,6) );    // direction for stepper
// TODO declare pins int PORT_PIN()

timer_msp432 * timer;
uart_msp432 * uart;
// TODO declare Stepper Class
// Stepper * stepper;

unsigned long long uptime = 0;
void setup(){
    // uart setup
    uart = new uart_msp432();
    uart->setBaudrate(115200);
    enable_pin.gpioMode( GPIO::OUTPUT );
    step_pin.gpioMode( GPIO::OUTPUT );
    dir_pin.gpioMode( GPIO::OUTPUT );

    // setup timer
    timer = new timer_msp432();
    timer->setCallback(tick, nullptr);
    timer->setPeriod(1000, TIMER::PERIODIC);
    timer->start();

    // TODO Create Stepper Class
    // stepper = new Stepper(200, step_pin, dir_pin);
}

void tick(void * arg) {
    uptime++;
}

void delay(int us) {
    int now = uptime;
    while((now - uptime) != us);
}

int fullEject() {
    dir_pin.gpioWrite(HIGH);
    enable_pin.gpioWrite(LOW);
    int x;
    for(x = 0; x < 600; ++x){ // 600 Steps 10.800 grad
        step(5000);
    }
    return x;
}

void step(int speed) {
    step_pin.gpioToggle();
    delay(speed);
}

int eject(bool & expired) {
    dir_pin.gpioWrite(HIGH);
    enable_pin.gpioWrite(LOW);
    int x = 0;
    while(!expired){
        step(5000);
        x++; //count steps
    }
    return x;
}

void interEject(int steps) {
    dir_pin.gpioWrite(LOW);
    enable_pin.gpioWrite(LOW);
    for (steps; steps > 0; steps--){
        step(10000);
    }
}

int main (void)
{
    while(true)
    {
        fullEject();
        delay(1000);
    }
}
