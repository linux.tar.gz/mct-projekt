/*------------------------------------------------------------------------------------------------------------

 Realisiert mit Tiny13
 
 Interner Oszillator mit rund 4MHz
 
 Layout:
                             1 �   8    +5V
   Beruehrung erkannt(PB3)   2     7    PB2 
                             3     6    PB1: C und Sensor
   GND                       4     5    PB0: C

 Input:
  PB0: Messspannung und Kondensator 
  PB1: Kondensator und Sensoranschluss
 Output:
  PB1: Kondensator und Sensor
  PB0: Kondensator
  PB3: Meldung: Beruehrung erkannt!
  
  
  Kondensator 33nF zwischen PB0 und PB1 
  Sensor an PB1
  
  Ablauf:
    1) PB1 und PB0 L       Kondensator leeren
       warte 1-10ms      
    2) PB1 offen, PB0 H    Ladung �bertragen
    3) PB0 offen
    4) PB1 L,  
    5) PB0 misst           Ladung messen
       Weiter bei 2 solange PB0 L
    6) gehe zu 1   
*/
//------------------------------------------------------------------------------------------------------------
void delay_250us(void){  //Die Zeit stimmt nich mal annaehernd!
static unsigned char i;
  i=100;
  do{
     asm volatile("NOP");
     asm volatile("NOP");
     asm volatile("NOP");
     asm volatile("NOP");

     asm volatile("NOP");
     asm volatile("NOP");
     asm volatile("NOP");
     asm volatile("NOP");
  }while (--i);  
}

//------------------------------------------------------------------------------------------------------------
int main(void){

  static int n;
  static long int summe;
  static int loops, grenze;
  static unsigned char i,init;
  
  cli();
  init=1;
  
  DDRB = 1|2|8|16;//PB0 | PB1 | PB3 | PB4; //PB 0,1,3,4 sind Ausgang
  PORTB = 0; //Kondensator leeren
  delay_250us(); 
  loops=0;
  summe=0;
  grenze=0;
 
  while(1){
    DDRB = 1|8|16;//PB0 | PB3 | PB4; //PB1 auf Eingang
    PORTB = 0; //Kein Pull-Up an PB1
    n=0;
    //Lade Kondensator und zaehle die Zyklen
    while(1){
      DDRB = 1|8|16;//PB0 auf Ausgang
      PORTB |= 1; //PB0 H
      asm volatile("NOP");
      asm volatile("NOP");
      asm volatile("NOP");
      n++;
      DDRB =   8|16; //PB0 wieder auf Eingang
      DDRB = 2|8|16; //PB1 jetzt auf Ausgang (PB0 auf Eingang)
      PORTB &= ~2; //PB1 L
      asm volatile("NOP");
      if (1 & PINB) break; //Messen
      DDRB = 8|16;//Beide auf Eingang
    }
    DDRB = 1|2|8|16;//PB0 | PB1 | PB3 | PB4; //PB 0,1,3,4 sind Ausgang

    //Kondensator leeren, Ber�hrung signalisieren
    if (n<grenze){
      PORTB=8;  //Ber�hrung entdeckt
    }else{
      PORTB=0;    //keine Ber�hrung entdeckt
    }

    if (init){
      summe+=n;
      loops++;
      if (loops>100){
        summe/=loops;
        grenze=(summe*96/100);
        loops=0;
        summe=0;
        init=0;
      }
    }

    for (i=0; i<200; i++){
      delay_250us(); 
    }
  }  
  return 0;
}
// EOF----------------------------------------------------------------------------------------------------------
