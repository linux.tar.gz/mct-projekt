#include "gpio_msp432.h"
#include "timer_msp432.h"

// Init
gpio_msp432_pin measure_pin(PORT_PIN());
gpio_msp432_pin capazitor_pin(PORT_PIN());

void delay(int us) {
    // TODO
}

int measure() {
    measure_pin.gpioMode( GPIO::INPUT );
    capazitor_pin.gpioMode( GPIO::OUTPUT );
    capazitor_pin.gpioWrite( LOW );
    return measure_pin.gpioRead();
}

void discharge() {
    measure_pin.gpioMode( GPIO::OUTPUT );
    measure_pin.gpioWrite(LOW);
    capazitor_pin.gpioMode( GPIO::OUTPUT );
    capazitor_pin.gpioWrite( LOW );

    delay(10000);
}

void charge() {
    capazitor_pin.gpioMode( GPIO::INPUT );
    measure_pin.gpioWrite( HIGH );

    // TODO capazitor_pin LOW or high
    // TODO delay charge time
}
