#ifndef Timer_h
#define Timer_h

class Timer {
public:
    Timer();
    unsigned long long getTime();
private:
    void tick(void * arg);
    unsigned long long uptime = 0;
    timer_msp432 * timer;
};
#endif
