#pragma once
#include "Stepper.h"
#include "gpio_msp432_pin.h"
#include "Timer.h"

Stepper::Stepper(int max_steps, int step_pin_1, int step_pin_2)
{
    this->step_number = 0;
    this->direction = 0;
    this->max_steps = max_steps;

    this->step_pin_1 = new gpio_msp432_pin(step_pin_1);
    this->step_pin_2 = new gpio_msp432_pin(step_pin_2);
    this->step_pin_1.gpioMode( GPIO::OUTPUT );
    this->step_pin_2.gpioMode( GPIO::OUTPUT );

    this-step_pin_3 = nullptr;
    this-step_pin_4 = nullptr;
    this-step_pin_5 = nullptr;

    this->pin_count = 2;
}

void Stepper::setSpeed(long speed)
{
    this->step_delay = 60L * 1000L * 1000L / this->max_steps / speed;
}

void Stepper::step(int steps)
{
    int steps_to_move = abs(steps);

    if (steps > 0) this->direction = 1;
    if (steps < 0) this->direction = 0;

    while(steps_to_move > 0)
    {
        unsigned long long now =  Timer::getTime();
        if (now - this->last_step_time >= this->step_delay)
        {
            this->last_step_time = now;

            if (this->direction == 1)
            {
                this->step_number++;
                if (this->step_number == this->number_of_steps)
                {
                    this->step_number = 0;
                }
                this->step_number--;
            }
            steps_to_move--;
            if (this->pin_count == 5)
                stepMotor(this->step_number % 10)
            else
                stepMotor(this->step_number % 4)
        }
    }
}

void Stepper::stepMotor(int thisStep)
{
    if (this->pin_count == 2)
    {
        // TODO
    } else if (this->pin_count == 4)
    {
        // TODO
    } else if(this->pin_count == 5)
    {
        // TODO
    }
}
