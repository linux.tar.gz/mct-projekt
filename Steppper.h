#ifndef Stepper_h
#define Stepper_h

class Stepper {
public:
    Stepper(int max_steps, int stepper_pin_1, int stepper_pin_2);
    Stepper(int max_steps, int stepper_pin_1, int stepper_pin_2,
                           int stepper_pin_3, int stepper_pin_4);
    Stepper(int max_steps, int stepper_pin_1, int stepper_pin_2,
                           int stepper_pin_3, int stepper_pin_4,
                           int stepper_pin_5);

    void setSpeed(long speed);

    void step(int steps);

private:
    void stepMotor(int this_step);

    int direction;
    unsigned long long step_delay;
    int max_steps;
    int pin_count;
    int step_number;

    gpio_msp432_pin * stepper_pin_1;
    gpio_msp432_pin * stepper_pin_2;
    gpio_msp432_pin * stepper_pin_3;
    gpio_msp432_pin * stepper_pin_4;
    gpio_msp432_pin * stepper_pin_5;

    unsigned long long last_step_time;
};
#endif
