#include "Timer.h"
#include "timer_msp432.h"

Timer::Timer()
{
    this->timer = new timer_msp432();
    this->timer.setCallback(tick, nullptr);
    this->timer.setPeriod(1000, TIMER::PERIODIC);
    this->timer.start();
}

void Timer::tick(void * arg)
{
    this->uptime++;
}

unsigned long long Timer::getTime()
{
    return this->uptime;
}
