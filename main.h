#pragma once
#include "gpio_msp432.h"
#include "uart_msp432.h"
#include "timer_msp432.h"


void setup() ;
void tick(void * arg);
void delay(int us);
int fullEject();
void step(int speed);
int eject(bool & expired);
void interEject(int steps);
